import h3
import geopy
import folium

import pandas as pd
from sklearn.pipeline import FeatureUnion

from collections import defaultdict


def plot_clusters(data, clusters, lat_column, lon_column, location=None,
                  show_clusters=9):
    if not location:
        city = "Doha"
        locator = geopy.geocoders.Nominatim(user_agent="MyCoder")
        location = locator.geocode(city)
        location = [location.latitude, location.longitude]
    map_ = folium.Map(location=location, tiles="cartodbpositron",
                      zoom_start=11)
    data['cluster'] = clusters

    colors = ['red', 'blue', 'green', 'purple', 'orange', 'darkred',
              'lightred', 'beige', 'darkblue', 'darkgreen', 'cadetblue',
              'darkpurple', 'white', 'pink', 'lightblue', 'lightgreen',
              'gray', 'black', 'lightgray']
    if show_clusters:
        mask = clusters >= 0
        cluster_hexs = data[mask].groupby(clusters[mask]).apply(
            coord2hex_cluster,
            h3_size=show_clusters,
            lat_column='PickupLatitude',
            lon_column='PickupLongitude'
        ).to_dict()
        if 'weight' in data:
            cluster_weight = data[mask].groupby(clusters[mask])['weight'] \
                                       .sum().to_dict()
        else:
            cluster_weight = defaultdict(int)

        for c in cluster_hexs.keys():
            for h in cluster_hexs[c]:
                folium.Polygon(
                    h3.h3_to_geo_boundary(h), fill=True,
                    fill_color=colors[c % len(colors)], weight=1,
                    color=colors[c % len(colors)],
                    popup=folium.Popup(
                        f'Cluster #{c} with {cluster_weight[c]} orders',
                        parse_html=True
                    )
                ).add_to(map_)

    if 'weight' in data:
        data.apply(lambda row: folium.CircleMarker(
            location=[row[lat_column], row[lon_column]],
            color=colors[int(row['cluster'] % len(colors))], fill=True,
            radius=2,
            tooltip=folium.Tooltip(f'Here are {row["weight"]} orders')
        ).add_to(map_), axis=1)
    else:
        data.apply(lambda row: folium.CircleMarker(
                location=[row[lat_column], row[lon_column]],
                color=colors[int(row['cluster'] % len(colors))], fill=True,
                radius=2).add_to(map_), axis=1)

    return map_


class PandasFeatureUnion(FeatureUnion):
    def fit_transform(self, X, y=None):
        result = [trans.fit_transform(X, y)
                  for name, trans, weight in self._iter()]
        return self.merge_dataframes_by_column(result)

    def merge_dataframes_by_column(self, Xs):
        return pd.concat(Xs, axis="columns", copy=False)

    def transform(self, X, y=None):
        result = [trans.transform(X, y)
                  for name, trans, weight in self._iter()]
        return self.merge_dataframes_by_column(result)
